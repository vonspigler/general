#!/usr/bin/env Rscript

library(ggplot2)
library(data.table)
library(argparse)


fancy_scientific2 <- function(l) {
  l <- format(l, scientific = TRUE)
  l <- gsub("^(.*)e", "e", l)
  l <- gsub("e", "10^", l)
  parse(text=l)
}

commandline_parser = ArgumentParser(description="plot log log data")
commandline_parser$add_argument(
    '-f',
    '--files',
    type='character',
    nargs='*',
    help='image'
)
args = commandline_parser$parse_args()


file.names = data.table(file.name=args$f)
table = file.names[, fread(file.name), by=file.name]
table[, label := strsplit(
    strsplit(file.name, "\\.")[[1]][1],
    "_")[[1]][2], by=file.name]
setnames(table, "V1", "DE")
setnames(table, "V2", "P")
print(table)


plot = ggplot(table) +
    geom_point(aes(x=DE, y=P, colour=label, shape=label)) +
    stat_function(fun=function(x) x^(-2/1.41)/30000.0) +
    scale_y_log10(labels=fancy_scientific2) +
    scale_x_log10(labels=fancy_scientific2) +
    xlab(expression(paste("|",Delta,"E|"))) +
    ylab("probability")


width = 20
factor = 0.618
height = factor * width
dev.new(width=width, height=height)
print(plot)
ggsave("plot.png", plot, width=width, height=height, dpi=300)
invisible(readLines("stdin", n=1))
